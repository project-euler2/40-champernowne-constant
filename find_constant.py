import time
start_time = time.time()

def brute_force_with_list(ranks):
    solution = 1
    for rank in ranks:
        value = ''.join([str(ints) for ints in range(1,rank+1,1)])
        solution *= int(value[rank-1])
    return solution
print(brute_force_with_list([1,10,100,1000,10000,100000,1000000]))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )